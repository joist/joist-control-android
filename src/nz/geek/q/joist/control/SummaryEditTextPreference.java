package nz.geek.q.joist.control;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;

public class SummaryEditTextPreference extends EditTextPreference {
	public SummaryEditTextPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onDialogClosed(boolean positiveResult) {
		super.onDialogClosed(positiveResult);
		setSummary(getSummary());
	}

	@Override
	public CharSequence getSummary() {
		return this.getText();
	}
}
