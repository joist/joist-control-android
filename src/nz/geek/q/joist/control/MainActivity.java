package nz.geek.q.joist.control;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity {
	private static final String TAG = "MainActivity";
	private static final Map<Integer, String> BUTTON_COMMANDS = new HashMap<>();
	static {
		BUTTON_COMMANDS.put(R.id.forwardLeftButton, "fl 1 200");
		BUTTON_COMMANDS.put(R.id.forwardButton, "f 1 1000");
		BUTTON_COMMANDS.put(R.id.forwardRightButton, "fr 1 200");
		BUTTON_COMMANDS.put(R.id.leftButton, "l 1 200");
		BUTTON_COMMANDS.put(R.id.rightButton, "r 1 200");
		BUTTON_COMMANDS.put(R.id.backLeftButton, "bl 1 200");
		BUTTON_COMMANDS.put(R.id.backButton, "b 1 1000");
		BUTTON_COMMANDS.put(R.id.backRightButton, "br 1 200");
	}

	private NetworkClient networkClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		networkClient = new NetworkClient(this);
		networkClient.connect();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.action_settings:
			Intent i = new Intent(this, SettingsActivity.class);
			startActivity(i);
			return true;
		case R.id.action_connect:
			networkClient.connect();
		case R.id.action_disconnect:
			networkClient.disconnect();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		networkClient.disconnect();
	}

	public void buttonClicked(View button) {
		int buttonId = button.getId();
		if (BUTTON_COMMANDS.containsKey(buttonId)) {
			networkClient.sendCommand(BUTTON_COMMANDS.get(buttonId));
		} else {
			Log.e(TAG, "Unexpected click on " + button);
		}
	}
}
