package nz.geek.q.joist.control;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Client to connect to the server over TCP, decode commands and send them to the command queue.
 */
public class NetworkClient implements Runnable {
	private static final String TAG = "NetworkClient";

	private final BlockingQueue<String> commandQueue = new LinkedBlockingQueue<>();
	private final Context context;

	private boolean running = false;

	public NetworkClient(Context context) {
		this.context = context;
	}

	@Override
	public void run() {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		String serverHost = sharedPref.getString(SettingsActivity.KEY_SERVER_HOST, "");
		int serverPort = Integer.parseInt(sharedPref.getString(SettingsActivity.KEY_SERVER_PORT, ""));

		// Connect
		Log.i(TAG, "Trying to connect to " + serverHost + ":" + serverPort);
		try (Socket socket = new Socket(serverHost, serverPort);
				OutputStreamWriter writer = new OutputStreamWriter(socket.getOutputStream())) {
			Log.i(TAG, "Connected: " + socket);
			postToast("Connected to " + serverHost + ":" + serverPort);
			running = true;
			// Wait for commands to send
			while (socket.isConnected() && running) {
				String command = commandQueue.poll(1, TimeUnit.SECONDS);
				if (command != null) {
					writer.append(command).append('\n').flush();
				}
			}
			running = false;
			postToast("Disconnected");
		} catch (IOException e) {
			Log.e(TAG, "Error connecting to server", e);
			postToast("Error connecting to server");
		} catch (InterruptedException e) {
			Log.e(TAG, "Error awaiting commands", e);
		}
	}

	private void postToast(final String message) {
		new Handler(Looper.getMainLooper()).post(new Runnable() {
			@Override public void run() {
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}
		});
	}

	public void sendCommand(String command) {
		commandQueue.add(command);
	}

	public void connect() {
		if (running) {
			Toast.makeText(context, "Already connected", Toast.LENGTH_LONG).show();
			return;
		}
		commandQueue.clear();
		new Thread(this).start();
	}

	public void disconnect() {
		running = false;
		commandQueue.clear();
	}
}
